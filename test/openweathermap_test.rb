# frozen_string_literal: true

require 'test_helper'

module Openweathermap
  class Test < ActiveSupport::TestCase
    test 'truth' do
      assert_kind_of Module, Openweathermap
    end
  end
end
