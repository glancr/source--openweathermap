# frozen_string_literal: true

require 'json'

$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'openweathermap/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'openweathermap'
  s.version     = Openweathermap::VERSION
  s.authors     = ['Tobias Grasse', 'Marco Roth']
  s.email       = %w(tg@glancr.de marco.roth@intergga.ch)
  s.homepage    = 'https://glancr.de/modules/openweathermap'
  s.summary     = 'mirr.OS data source for openweathermap.org.'
  s.description = 'Get weather forecast data from Openweathermap.org.'
  s.license     = 'MIT'
  s.metadata    = {
    'json' => {
      type: 'sources',
      title: {
        enGb: 'Openweathermap'
      },
      description: {
        enGb: s.description,
        deDe: 'Lade Wettervorhersage-Daten von Openweathermap.org.',
        frFr: 'Obtenez les données des prévisions météorologiques sur Openweathermap.org.',
        esEs: 'Obtenga los datos del pronóstico del tiempo en Openweathermap.org.',
        plPl: 'Uzyskaj dane pogodowe z Openweathermap.org.',
        koKr: 'Openweathermap.org에서 일기 예보 데이터를 다운로드하십시오.'
      },
      groups: %w(weather_owm current_weather),
      compatibility: '0.10.0'
    }.to_json
  }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'httparty', '~> 0.18'
  s.add_development_dependency 'rails'
  s.add_development_dependency 'rubocop', '~> 0.80'
  s.add_development_dependency 'rubocop-rails'
end
