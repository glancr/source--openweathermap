# frozen_string_literal: true

module Openweathermap
  VERSION = '1.2.0'
end
